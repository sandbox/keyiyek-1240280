Readme file for the Book Enhance module by keyiyek hrs@libero.it

This tiny module enhances the configuration options for the book module.
It gives the chance to set a defauld child for every content type that are set
for book outlines.

Why this module?
the original book module allowes only one content type to be set as child page.
This content type is the one called with the "add child page" link under every
node of a content type that is allowed to be a book outline.

In the case you want to have more than one content page to be allowed the book
outline, you may also want to have the link "add child page" pointing to
different content types depending on the parent type.
e.g. If I want to have a hierarchical structure with book pages, but also one
structure with some other content type of yours (let say people of a family),
you can, with this module, have an "add child page" that points to a book page
in the first case and to a "people of a family" in the other.
You can even mix tham up, this is up to you.